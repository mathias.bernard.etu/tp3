import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component.js';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new Component(
		'section',
		null,
		'Ici vous pourrez ajouter une pizza'
	);
Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];
Router.menuElement = document.querySelector('.mainMenu');

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas
const slog = document.querySelector('.logo');
slog.innerHTML += "<small>les pizzas c'est la vie</small>";
const laCarte = document.querySelector('.pizzaListLink');
const news = document.querySelector('.newsContainer');
news.setAttribute('style', 'display');
const ajout = document.querySelector('.aboutLink');
