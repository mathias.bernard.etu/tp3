export default class Router {
	static titleElement;
	static contentElement;
	/**
	 * Tableau des routes/pages de l'application.
	 * @example `Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }]`
	 */
	static routes = [];

	/**
	 * Affiche la page correspondant à `path` dans le tableau `routes`
	 * @param {String} path URL de la page à afficher
	 */
	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			// affichage du titre de la page
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			// affichage de la page elle même
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}

	static #menuElement; // propriété statique privée
	/**
	 * Indique au Router la balise HTML contenant le menu de navigation
	 * Écoute le clic sur chaque lien et déclenche la méthode navigate
	 * @param element Élément HTML qui contient le menu principal
	 */
	static set menuElement(element) {
		// setter
		this.#menuElement = element;
		element.array.forEach(element => {
			element.addEventListener('clic', e => {
				e.preventDefault();
				this.navigate(element.get);
			});
		});
		this.element.addEventListener('click', r => {
			this.navigate(routes[0]);
			console.log(routes[0]);
		});
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
	}
}
